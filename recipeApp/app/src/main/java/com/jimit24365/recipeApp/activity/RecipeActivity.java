package com.jimit24365.recipeApp.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aigestudio.wheelpicker.WheelPicker;
import com.bumptech.glide.Glide;
import com.jimit24365.recipeApp.R;
import com.jimit24365.recipeApp.utils.CommonUtilities;
import com.jimit24365.recipeApp.utils.CustomDialogPopOver;
import com.jimit24365.recipeApp.utils.ImagePickerUtility;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnDismissListener;

import java.util.ArrayList;

import jp.wasabeef.glide.transformations.BlurTransformation;

public class RecipeActivity extends AppCompatActivity implements View.OnClickListener {

    // constants
    final static String KEY_PROFICIENCY_STATE = "last_proficiency_state";
    final static String KEY_RECIPE_TYPE_STATE = "last_recipe_type_state";
    final static String KEY_PAX_STATE = "last_pax_state";
    final static String KEY_COOKING_TIME_STATE = "last_cooking_time_state";

    // Enum defining type of single wheel picker
    private enum CurrentOpenDialogType {
        RecipeTypeSelector,
        TotalCapacitySelector
    }

    //TypeFace to change font family of edit Text and buttons
    private CurrentOpenDialogType currentOpenDialogType;
    private Typeface normalTypeFace;
    private Typeface boldTypeFace;

    //Screen Views
    private ImageView mRecipeImageView;
    private ImageView mCameraButton;
    private ImageView recipeCookingTimeIcon;
    private ImageView paxIcon;
    private ImageView nextBtn;
    private EditText recipeNameEdt;
    private EditText recipeStoryEdt;
    private TextView recipeTypeTv;
    private TextView noOfPaxTv;
    private TextView screenCancelBtn;
    private TextView cookingTimeTv;
    private TextView nextBtnTv;
    private Button beginnerLevelBtn;
    private Button sousLevelBtn;
    private Button masterLevelBtn;

    private CustomDialogPopOver customDialogPopOver;

    private  int currentProficiencyLevel = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);
        mRecipeImageView = (ImageView) findViewById(R.id.recipe_background_img);
        mCameraButton = (ImageView) findViewById(R.id.camera_btn);
        paxIcon = (ImageView) findViewById(R.id.people_icon);
        nextBtn = (ImageView) findViewById(R.id.next_btn);
        recipeCookingTimeIcon = (ImageView) findViewById(R.id.clock_icon);
        recipeNameEdt = (EditText) findViewById(R.id.recipe_name_edt);
        recipeStoryEdt = (EditText) findViewById(R.id.story_edit);
        recipeTypeTv = (TextView) findViewById(R.id.recipe_type_tv);
        noOfPaxTv = (TextView) findViewById(R.id.servers_tv);
        nextBtnTv = (TextView) findViewById(R.id.next_btn_tv);
        cookingTimeTv = (TextView) findViewById(R.id.time_tv);
        masterLevelBtn = (Button) findViewById(R.id.proficiency_master_btn);
        beginnerLevelBtn = (Button) findViewById(R.id.proficiency_beginner_btn);
        sousLevelBtn = (Button) findViewById(R.id.proficiency_sous_btn);

        initUI();
        initListeners();
        if(savedInstanceState != null){
         restoreScreenState(savedInstanceState);
        }
    }

    // Initializing default states of views in screen
    private void initUI() {
        Glide.with(this)
                .load(R.drawable.recipe_dummy_background)
                .bitmapTransform(
                        new BlurTransformation(RecipeActivity.this)).into(mRecipeImageView);
        normalTypeFace = Typeface.createFromAsset(getAssets(),
                "fonts/HelveticaNeue-Normal.ttf");
        boldTypeFace = Typeface.createFromAsset(getAssets(),
                "fonts/HelveticaNeue-Medium.ttf");
        recipeNameEdt.setTypeface(normalTypeFace);
        recipeStoryEdt.setTypeface(normalTypeFace);
        beginnerLevelBtn.setTypeface(boldTypeFace);
        sousLevelBtn.setTypeface(boldTypeFace);
        masterLevelBtn.setTypeface(boldTypeFace);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_toolbar);
        screenCancelBtn = (TextView) findViewById(R.id.cancel_btn);
        recipeNameEdt.requestFocus();
    }

    //Initializing default listeners of views in screen
    private void initListeners() {
        mCameraButton.setOnClickListener(RecipeActivity.this);
        beginnerLevelBtn.setOnClickListener(RecipeActivity.this);
        sousLevelBtn.setOnClickListener(RecipeActivity.this);
        masterLevelBtn.setOnClickListener(RecipeActivity.this);
        recipeTypeTv.setOnClickListener(RecipeActivity.this);
        noOfPaxTv.setOnClickListener(RecipeActivity.this);
        paxIcon.setOnClickListener(RecipeActivity.this);
        recipeStoryEdt.setOnClickListener(RecipeActivity.this);
        nextBtnTv.setOnClickListener(RecipeActivity.this);
        nextBtn.setOnClickListener(RecipeActivity.this);
        cookingTimeTv.setOnClickListener(RecipeActivity.this);
        recipeCookingTimeIcon.setOnClickListener(RecipeActivity.this);
        screenCancelBtn.setOnClickListener(RecipeActivity.this);
        recipeStoryEdt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    String currentStory = String.valueOf(((EditText) v).getText());
                    if(TextUtils.isEmpty(currentStory)){
                            CommonUtilities.setStoryBoardProperty(v,false);
                    }
                }else{
                    CommonUtilities.setStoryBoardProperty(v,true);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        switch (viewId) {
            case R.id.camera_btn:
                ImagePickerUtility.checkPermissionAndGetImage(RecipeActivity.this);
                break;
            case R.id.proficiency_beginner_btn:
                changeProficiencyLevel(1);
                break;
            case R.id.proficiency_sous_btn:
                changeProficiencyLevel(2);
                break;
            case R.id.proficiency_master_btn:
                changeProficiencyLevel(3);
                break;
            case R.id.recipe_type_tv:
                showSingleWheelPicker(CurrentOpenDialogType.RecipeTypeSelector);
                break;
            case R.id.people_icon:
            case R.id.servers_tv:
                showSingleWheelPicker(CurrentOpenDialogType.TotalCapacitySelector);
                break;
            case R.id.next_btn:
            case R.id.next_btn_tv:
                handleNxtBtnClickEvent();
                break;
            case R.id.time_tv:
            case R.id.clock_icon:
                handleCookingTimeRequest();
                break;
            case R.id.cancel_btn:
                finish();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ImagePickerUtility.handleOnActivityResult(requestCode, resultCode, data, RecipeActivity.this, mRecipeImageView);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        ImagePickerUtility.handleOnRequestPermissionsResult(requestCode, permissions, grantResults, RecipeActivity.this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        saveScreenState(outState);
    }

    // Shows wheel picker in dialog box to pick time taken for recipe
    private void handleCookingTimeRequest() {
        final ArrayList<String>[] hoursList = new ArrayList[1];
        final ArrayList<String>[] minsList = new ArrayList[1];
        hoursList[0] = new ArrayList<>();
        minsList[0] = new ArrayList<>();
        for(int i = 0;i<=10;i++){
            hoursList[0].add(i+"");
        }
        for(int i = 0;i<=59;i++){
            minsList[0].add(i+"");
        }
        View mainContainer = RecipeActivity.this.getLayoutInflater().inflate(R.layout.cooking_time_picker, null);
        final WheelPicker hourPicker = (WheelPicker) mainContainer.findViewById(R.id.hour_picker);
        final WheelPicker minPicker = (WheelPicker) mainContainer.findViewById(R.id.min_picker);
        TextView cancelBtn = (TextView) mainContainer.findViewById(R.id.time_picker_cancel_btn);
        TextView doneBtn = (TextView) mainContainer.findViewById(R.id.time_picker_done_btn);
        hourPicker.setData(hoursList[0]);
        minPicker.setData(minsList[0]);
        if(cookingTimeTv != null){
            String cookingTimeValue = (String) cookingTimeTv.getText();
            if(!TextUtils.isEmpty(cookingTimeValue)){
                String hoursSymbol = RecipeActivity.this.getResources().getString(R.string.cooking_time_hours);
                String minSymbol = RecipeActivity.this.getResources().getString(R.string.cooking_time_min);
                String hours = cookingTimeValue.substring(0,cookingTimeValue.indexOf(hoursSymbol));
                String mins = cookingTimeValue.substring(cookingTimeValue.indexOf(hoursSymbol)+7,cookingTimeValue.indexOf(minSymbol));
                hours = hours.trim();
                mins = mins.trim();
                int hour = Integer.parseInt(hours);
                int minutes = Integer.parseInt(mins);
                hourPicker.setSelectedItemPosition(hour);
                minPicker.setSelectedItemPosition(minutes);
            }
        }
        customDialogPopOver = new CustomDialogPopOver(mainContainer);
        customDialogPopOver.showDialogPlusPopOver(RecipeActivity.this,false);
        View.OnClickListener cookingTimePopOverClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int viewId = v.getId();
                if(viewId == R.id.time_picker_done_btn){
                    String selectedHour = String.valueOf(hoursList[0].get(hourPicker.getCurrentItemPosition()));
                    String selectedMin = String.valueOf(minsList[0].get(minPicker.getCurrentItemPosition()));
                    String cookingFormatString = RecipeActivity.this.getResources().getString(R.string.cooking_time_formated_text);
                    String cookingTime = String.format(cookingFormatString,selectedHour,selectedMin);
                    cookingTimeTv.setText(cookingTime);
                    cookingTimeTv.setError(null);
                }
                customDialogPopOver.hideDialogPlusPopOver();
                hoursList[0] = null;
                minsList[0] = null;
            }
        };
        cancelBtn.setOnClickListener(cookingTimePopOverClickListener);
        doneBtn.setOnClickListener(cookingTimePopOverClickListener);
    }

    // Showing dialog box with success message
    private  void handleNxtBtnClickEvent() {
        if (recipeNameEdt != null && recipeTypeTv != null && noOfPaxTv != null && cookingTimeTv != null && recipeStoryEdt != null) {
            String recipeTitle = String.valueOf(recipeNameEdt.getText());
            String recipeTypeValue = String.valueOf(recipeTypeTv.getText());
            String recipePax = String.valueOf(noOfPaxTv.getText());
            String recipeTime = String.valueOf(cookingTimeTv.getText());
            String recipeStory = String.valueOf(recipeStoryEdt.getText());
            String errorString = RecipeActivity.this.getResources().getString(R.string.error_message);
            if (TextUtils.isEmpty(recipeTitle)) {
                recipeNameEdt.setError(errorString);
                return;
            }
            if (TextUtils.isEmpty(recipeTypeValue)) {
                recipeTypeTv.setError(errorString);
                return;
            }
            if (TextUtils.isEmpty(recipePax)) {
                noOfPaxTv.setError(errorString);
                return;
            }
            if(TextUtils.isEmpty(recipeTime)) {
                cookingTimeTv.setError(errorString);
                return;
            }
            if(TextUtils.isEmpty(recipeStory)) {
                recipeStoryEdt.setError(errorString);
                return;
            }
            View mainContainer = RecipeActivity.this.getLayoutInflater().inflate(R.layout.success_layout, null);
            final View[] headerView = {RecipeActivity.this.getLayoutInflater().inflate(R.layout.image_cropper_header, null)};
            ImageView headerCloseBtn = (ImageView) headerView[0].findViewById(R.id.header_close_btn);
            customDialogPopOver = new CustomDialogPopOver(mainContainer);
            customDialogPopOver.showDialogPlusPopOver(RecipeActivity.this, false, headerView[0], new OnDismissListener() {
                @Override
                public void onDismiss(DialogPlus dialog) {
                    headerView[0] = null;
                }
            });
            headerCloseBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    customDialogPopOver.hideDialogPlusPopOver();
                }
            });
        }
    }

    /**
     * Displays single wheel picker using dialog box
     * @param dialogType Is used to determine type and value of data in wheel-picker
     */
    private void showSingleWheelPicker(CurrentOpenDialogType dialogType) {
        View mainContainer = RecipeActivity.this.getLayoutInflater().inflate(R.layout.single_wheel_picker_layout, null);
        final TextView cancelBtn = (TextView) mainContainer.findViewById(R.id.time_picker_cancel_btn);
        final TextView doneBtn = (TextView) mainContainer.findViewById(R.id.time_picker_done_btn);
        final WheelPicker dataPicker = (WheelPicker) mainContainer.findViewById(R.id.common_wheel_picker);
        currentOpenDialogType = dialogType;
        final ArrayList<String> listOfRecipes = new ArrayList<>();
        switch (currentOpenDialogType){

            case RecipeTypeSelector:
                listOfRecipes.clear();
                listOfRecipes.add("Breakfast");
                listOfRecipes.add("Lunch");
                listOfRecipes.add("Brunch");
                listOfRecipes.add("Dinner");
                listOfRecipes.add("Salads");
                break;
            case TotalCapacitySelector:
                String paxValue = "";
                for ( int i = 1 ; i <= 10;i++){
                    if(i == 10){
                        paxValue = i+"+";
                    }else{
                        paxValue = i+"";
                    }
                    listOfRecipes.add(paxValue);
                }
                break;
        }

        dataPicker.setData(listOfRecipes);
        switch (currentOpenDialogType){

            case RecipeTypeSelector:
                String recipeType = (String) recipeTypeTv.getText();
                if(!TextUtils.isEmpty(recipeType)){
                   dataPicker.setSelectedItemPosition(listOfRecipes.indexOf(recipeType));
                }else{
                    dataPicker.setSelectedItemPosition(3);
                }
                break;
            case TotalCapacitySelector:
                String paxNumber = (String) noOfPaxTv.getText();
                if(!TextUtils.isEmpty(paxNumber)){
                    dataPicker.setSelectedItemPosition(listOfRecipes.indexOf(paxNumber));
                }else{
                    dataPicker.setSelectedItemPosition(3);
                }
                break;
        }
        customDialogPopOver = new CustomDialogPopOver(mainContainer);
        customDialogPopOver.showDialogPlusPopOver(RecipeActivity.this,false);
        View.OnClickListener singleDataPopOverClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int viewId = v.getId();
                if (viewId == R.id.time_picker_done_btn) {
                    int currentPosition = dataPicker.getCurrentItemPosition();
                    switch (currentOpenDialogType) {

                        case RecipeTypeSelector:
                            if (recipeTypeTv != null) {
                                recipeTypeTv.setText((String) listOfRecipes.get(currentPosition));
                                recipeTypeTv.setError(null);
                            }
                            break;
                        case TotalCapacitySelector:
                            if (noOfPaxTv != null) {
                                noOfPaxTv.setText((String) listOfRecipes.get(currentPosition));
                                noOfPaxTv.setError(null);
                            }
                            break;
                    }

                }
                customDialogPopOver.hideDialogPlusPopOver();
            }
        };
        cancelBtn.setOnClickListener(singleDataPopOverClickListener);
        doneBtn.setOnClickListener(singleDataPopOverClickListener);
    }

    /**
     * Its changes button and its text styles
     * @param level Is used to determine level of proficiency
     */
    private void changeProficiencyLevel(int level) {
        if (beginnerLevelBtn != null && sousLevelBtn != null && masterLevelBtn != null) {
            switch (level) {
                case 1:
                    beginnerLevelBtn.setBackgroundResource(R.drawable.rounded_btn);
                    sousLevelBtn.setBackgroundResource(R.drawable.rounded_btn_white_backgound);
                    masterLevelBtn.setBackgroundResource(R.drawable.rounded_btn_white_backgound);

                    beginnerLevelBtn.setTextColor(RecipeActivity.this.getResources().getColor(R.color.white));
                    sousLevelBtn.setTextColor(RecipeActivity.this.getResources().getColor(R.color.chefling_text_color));
                    masterLevelBtn.setTextColor(RecipeActivity.this.getResources().getColor(R.color.chefling_text_color));
                    break;
                case 2:
                    beginnerLevelBtn.setBackgroundResource(R.drawable.rounded_btn_white_backgound);
                    sousLevelBtn.setBackgroundResource(R.drawable.rounded_btn);
                    masterLevelBtn.setBackgroundResource(R.drawable.rounded_btn_white_backgound);

                    beginnerLevelBtn.setTextColor(RecipeActivity.this.getResources().getColor(R.color.chefling_text_color));
                    sousLevelBtn.setTextColor(RecipeActivity.this.getResources().getColor(R.color.white));
                    masterLevelBtn.setTextColor(RecipeActivity.this.getResources().getColor(R.color.chefling_text_color));
                    break;
                case 3:
                    beginnerLevelBtn.setBackgroundResource(R.drawable.rounded_btn_white_backgound);
                    sousLevelBtn.setBackgroundResource(R.drawable.rounded_btn_white_backgound);
                    masterLevelBtn.setBackgroundResource(R.drawable.rounded_btn);

                    beginnerLevelBtn.setTextColor(RecipeActivity.this.getResources().getColor(R.color.chefling_text_color));
                    sousLevelBtn.setTextColor(RecipeActivity.this.getResources().getColor(R.color.chefling_text_color));
                    masterLevelBtn.setTextColor(RecipeActivity.this.getResources().getColor(R.color.white));
                    break;
            }
            currentProficiencyLevel = level;
        }
    }

    /**
     * Save all screen views(Which are not handle by android) values when activity is destroying
     * @param outState bundle to save data into
     */
    private void saveScreenState(Bundle outState){
        String recipeType = (String) recipeTypeTv.getText();
        String noOfPax = (String) noOfPaxTv.getText();
        String cookingTime = (String) cookingTimeTv.getText();

        outState.putInt(KEY_PROFICIENCY_STATE, currentProficiencyLevel);
        outState.putString(KEY_PAX_STATE, noOfPax);
        outState.putString(KEY_COOKING_TIME_STATE, cookingTime);
        outState.putString(KEY_RECIPE_TYPE_STATE, recipeType);
        ImagePickerUtility.storeState(outState);
    }

    /**
     * Restores all values from saved bundle to respected views
     * @param savedInstanceState bundle to get saved data
     */
    private void restoreScreenState(Bundle savedInstanceState){
        String recipeType = savedInstanceState.getString(KEY_RECIPE_TYPE_STATE);
        String noOfPax = savedInstanceState.getString(KEY_PAX_STATE);
        String cookingTime = savedInstanceState.getString(KEY_COOKING_TIME_STATE);

        if(!TextUtils.isEmpty(recipeType)){
         recipeTypeTv.setText(recipeType);
        }
        if(!TextUtils.isEmpty(noOfPax)){
            noOfPaxTv.setText(noOfPax);
        }
        if(!TextUtils.isEmpty(cookingTime)){
            cookingTimeTv.setText(cookingTime);
        }
        int savedProficiencyLevel = savedInstanceState.getInt(KEY_PROFICIENCY_STATE);
        changeProficiencyLevel(savedProficiencyLevel);
        ImagePickerUtility.restoreImageViewState(
                RecipeActivity.this,
                mRecipeImageView,savedInstanceState);
    }
}
