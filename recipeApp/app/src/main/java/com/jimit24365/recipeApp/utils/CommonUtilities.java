package com.jimit24365.recipeApp.utils;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

/**
 * Created by jimit on 23/10/16.
 */

public class CommonUtilities {

    /**
     * Converts dotPerPixel value into pixels
     * @param dp value in dot per pixel format
     * @param context activity reference to get context
     * @return values in pixels format
     */
    private static int dpToPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    /**
     * Switch editext height from wrap content to fix-size and vice versa, based on flag
     * @param viewToModify edit text to be modified
     * @param shouldMakeWrapText flag to determine wraping is required or not
     */
    public static void setStoryBoardProperty(View viewToModify, boolean shouldMakeWrapText){
        ViewGroup.LayoutParams layoutParams = viewToModify.getLayoutParams();
        if(shouldMakeWrapText){
            layoutParams.height =WRAP_CONTENT;
        }else{
            layoutParams.height = dpToPx(40,viewToModify.getContext());
        }
        viewToModify.setLayoutParams(layoutParams);
        viewToModify.requestLayout();
    }
}
