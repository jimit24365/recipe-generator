package com.jimit24365.recipeApp.utils;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.jimit24365.recipeApp.R;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jp.wasabeef.glide.transformations.BlurTransformation;

import static android.app.Activity.RESULT_OK;

/**
 * Created by recipeApp on 22/10/16.
 */

public class ImagePickerUtility {


    private static final int MY_PERMISSIONS_REQUEST_READ_WRITE = 1001;
    private static final int REQUEST_CODE = 200;
    private static final String KEY_IMAGE_VIEW_FILE = "image_view_file";
    private static File finalFile;

    /**
     * Get the URI of the selected image from  {@link #getPickImageResultUri(Intent, Activity)}.<br/>
     * Will return the correct URI for camera  and gallery image.
     *
     * @param data the returned data of the  activity result
     */
    private static Uri getPickImageResultUri(Intent data, Activity activityContext) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri(activityContext) : data.getData();
    }

    /**
     * Get URI to image received from capture  by camera.
     */
    private static Uri getCaptureImageOutputUri(Activity activityContext) {
        Uri outputFileUri = null;
        File getImage = activityContext.getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    private static Intent getPickImageChooserIntent(Activity activityContext) {

// Determine Uri of camera image to  save.
        Uri outputFileUri = getCaptureImageOutputUri(activityContext);

        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager = activityContext.getPackageManager();

// collect all camera intents
        Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
            }
            allIntents.add(intent);
        }

// collect all gallery intents
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        List<ResolveInfo> listGallery = packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new Intent(galleryIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }

// the main intent is the last in the  list  so pickup the useless one
        Intent mainIntent = allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity")) {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

// Create a chooser from the main  intent
        Intent chooserIntent = Intent.createChooser(mainIntent, "Select source");

// Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }


    private static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        ContentResolver contentResolver = inContext.getContentResolver();
        String dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(new Date());
        String path = MediaStore.Images.Media.insertImage(contentResolver, inImage, "Recipe" + dateFormat, "Recipe Image");
        if (!TextUtils.isEmpty(path)) {
            return Uri.parse(path);
        } else {
            return null;
        }
    }

    private static String getRealPathFromURI(Uri uri, Activity activityContext) {
        if (uri != null) {
            Cursor cursor = activityContext.getContentResolver().query(uri, null, null, null, null);
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        } else {
            return null;
        }
    }

    private static void fixMediaDir() {
        File sdcard = Environment.getExternalStorageDirectory();
        if (sdcard != null) {
            File mediaDir = new File("/sdcard/Pictures");
            if (!mediaDir.exists()) {
                mediaDir.mkdirs();
            }
        }
    }

    public static void checkPermissionAndGetImage(Activity activityContext) {
        if (ContextCompat.checkSelfPermission(activityContext,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(activityContext,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activityContext,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_WRITE);

        } else {
            activityContext.startActivityForResult(getPickImageChooserIntent(activityContext), REQUEST_CODE);
        }
    }

    public static void handleOnRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults, Activity activityContext) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_WRITE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    activityContext.startActivityForResult(getPickImageChooserIntent(activityContext), REQUEST_CODE);
                }
                return;
            }
        }
    }

    public static void handleOnActivityResult(int requestCode, int resultCode, Intent data, final Activity activityContext, final ImageView view) {
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            final Uri imageUri = getPickImageResultUri(data, activityContext);


            View headerView = activityContext.getLayoutInflater().inflate(R.layout.image_cropper_header, null);
            View mainContainer = activityContext.getLayoutInflater().inflate(R.layout.image_profile_cropper, null);

            final CropImageView cropImageView = (CropImageView) mainContainer.findViewById(R.id.CropImageView);
            final View cancleBtn = mainContainer.findViewById(R.id.cancel_crop);
            final View submitBtn = mainContainer.findViewById(R.id.crop_and_save);

            TextView headerText = (TextView) headerView.findViewById(R.id.header_title);
            ImageView headerCloseBtn = (ImageView) headerView.findViewById(R.id.header_close_btn);

            headerText.setText("Please Crop");
            cropImageView.setFixedAspectRatio(true);
            cropImageView.setAspectRatio(150, 100);
            cropImageView.setCropShape(CropImageView.CropShape.RECTANGLE);
            cropImageView.setGuidelines(CropImageView.Guidelines.OFF);
            cropImageView.setImageUriAsync(imageUri);
            cropImageView.setScaleType(CropImageView.ScaleType.FIT_CENTER);

            final DialogPlus mDialogPlus = DialogPlus.newDialog(activityContext)
                    .setHeader(headerView)
                    .setContentHolder(new ViewHolder(mainContainer))
                    .setInAnimation(R.anim.slide_in_top)
                    .setOutAnimation(R.anim.slide_out_bottom)
                    .setGravity(Gravity.CENTER)
                    .setMargin(20,20,20,20)
                    .setCancelable(true)
                    .create();
            mDialogPlus.show();


            View.OnClickListener profilePickerCloseBanListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int viewId = v.getId();
                    if (viewId == R.id.header_close_btn || viewId == R.id.cancel_crop) {
                        if (mDialogPlus != null && mDialogPlus.isShowing()) {
                            mDialogPlus.dismiss();
                        }
                    } else if (viewId == R.id.crop_and_save) {
                        Bitmap cropped = cropImageView.getCroppedImage(150, 100);
                        if (cropped != null) {
                            // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
                            fixMediaDir();
                            Uri tempUri = getImageUri(activityContext.getApplicationContext(), cropped);

                            // CALL THIS METHOD TO GET THE ACTUAL PATH
                            String filePath = getRealPathFromURI(tempUri, activityContext);
                             finalFile = new File(getRealPathFromURI(tempUri, activityContext));

                            setImage(activityContext,view);

                        }
                        if (mDialogPlus != null && mDialogPlus.isShowing()) {
                            mDialogPlus.dismiss();
                        }
                    }
                }
            };
            headerCloseBtn.setOnClickListener(profilePickerCloseBanListener);
            cancleBtn.setOnClickListener(profilePickerCloseBanListener);
            submitBtn.setOnClickListener(profilePickerCloseBanListener);
        }
    }


    private static void setImage(Activity activityContext, ImageView imageView){
        if (finalFile != null) {
            Glide.with(activityContext).load(finalFile)
                    .bitmapTransform(new BlurTransformation(activityContext))
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
        }
    }

    public static void storeState(Bundle outState){
        outState.putSerializable(KEY_IMAGE_VIEW_FILE,finalFile);
    }

    public static void restoreImageViewState(Activity activityContext,ImageView imageView,Bundle savedState){
        if(savedState != null){
            finalFile = (File) savedState.getSerializable(KEY_IMAGE_VIEW_FILE);
            setImage(activityContext,imageView);
        }
    }

}
