package com.jimit24365.recipeApp.utils;

import android.app.Activity;
import android.view.Gravity;
import android.view.View;

import com.jimit24365.recipeApp.R;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnDismissListener;
import com.orhanobut.dialogplus.ViewHolder;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

/**
 * Created by jimit on 22/10/16.
 */

public class CustomDialogPopOver {

    private DialogPlus dialogPlus;
    private View mainContainerView;

    public CustomDialogPopOver(View mainContainerView){
        this.mainContainerView = mainContainerView;
    }

    public   void showDialogPlusPopOver(Activity activityContext,boolean shouldHaveTopMargin){
        dialogPlus = DialogPlus.newDialog(activityContext)
                .setContentHolder(new ViewHolder(mainContainerView))
                .setInAnimation(R.anim.slide_in_top)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setGravity(Gravity.CENTER_VERTICAL)
                .setContentHeight(600)
                .setContentWidth(MATCH_PARENT)
                .setMargin(20, shouldHaveTopMargin ? 20:0, 20, 20)
                .setCancelable(true)
                .create();
        dialogPlus.show();
    }

    public  void showDialogPlusPopOver(Activity activityContext, boolean shouldHaveTopMargin, View headerView,OnDismissListener onDismissListener){
        dialogPlus = DialogPlus.newDialog(activityContext)
                .setHeader(headerView)
                .setContentHolder(new ViewHolder(mainContainerView))
                .setInAnimation(R.anim.slide_in_top)
                .setOutAnimation(R.anim.slide_out_bottom)
                .setGravity(Gravity.CENTER_VERTICAL)
                .setContentHeight(600)
                .setContentWidth(MATCH_PARENT)
                .setMargin(20, shouldHaveTopMargin ? 20:0, 20, 20)
                .setOnDismissListener(onDismissListener)
                .setCancelable(true)
                .create();
        dialogPlus.show();
    }

    public   void hideDialogPlusPopOver(){
        if (dialogPlus != null) {
            dialogPlus.dismiss();
            dialogPlus = null;
        }
        mainContainerView = null;
    }
}
